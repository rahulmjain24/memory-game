const gameContainer = document.querySelector("#game")

const reset = document.querySelector('.reset')

const currScore = document.querySelector('.current')
const highest = document.querySelector('.highest')

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
]

const GIFS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9]
// const GIFS = [1,2,1,2]

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter)

    // Decrease counter by 1
    counter--

    // And swap the last element with it
    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

let shuffledColors = shuffle(COLORS)

let shuffledGifs = shuffle(GIFS)

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {

  for (let colorIndex = 0; colorIndex < colorArray.length; colorIndex++) {
    // create a new div
    const newDiv = document.createElement("div")

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(colorArray[colorIndex])
    newDiv.id = colorIndex
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick)
    // append the div to the element with an id of game
    gameContainer.appendChild(newDiv)
  }
}

function createDivForGifs(gifsArray) {
  for (let gifIndex = 0; gifIndex < gifsArray.length; gifIndex++) {
    const newDiv = document.createElement("div")
    const content = document.createElement('div')
    const front = document.createElement('div')
    const back = document.createElement('div')

    newDiv.classList.add('card')
    newDiv.id = gifIndex

    content.classList.add('content')

    front.classList.add('front')
    front.textContent = '😍'

    back.classList.add('back')
    back.style.background = `url(gifs/${gifsArray[gifIndex]}.gif) 0 0/ cover no-repeat`
    back.style.backgroundPosition = 'center'

    content.appendChild(front)
    content.appendChild(back)
    newDiv.appendChild(content)

    newDiv.addEventListener("click", handleCardClickForGif)

    gameContainer.appendChild(newDiv)
  }
}

let clickCounter = 0
let prevEvent = null
let memorized = []
let score = 0

// TODO: Implement this function! 
function handleCardClick(event) {
  // you can use event.target to see which element was clicked 
  if (clickCounter < 2 && event.target !== prevEvent && !memorized.includes(event.target.id)) {
    event.target.style.background = event.target.classList[0]

    clickCounter++

    if (clickCounter === 1) {
      prevEvent = event.target
    }

    if (clickCounter === 2) {
      if (event.target.classList[0] === prevEvent.classList[0]) {
        memorized.push(event.target.id)
        memorized.push(prevEvent.id)
        clickCounter = 0
      } else {
        setTimeout(() => {
          event.target.style.background = ''
          prevEvent.style.background = ''
          clickCounter = 0
        }, 1000)
      }
    }
  }
}

function handleCardClickForGif(event) {
  // you can use event.target to see which element was clicked 
  // support for firefox
  const storedEvent = event.path || event.composedPath()
  if (clickCounter < 2 && storedEvent.at(-6) !== prevEvent && !memorized.includes(storedEvent.at(-6).id)) {
    event.target.parentElement.style.transform = 'rotateY(-180deg)'

    clickCounter++
    score++

    currScore.textContent = score
    highest.textContent = localStorage.getItem('highest') || '0'

    if (clickCounter === 1) {
      prevEvent = storedEvent.at(-6)
    }

    if (clickCounter === 2) {
      if (parseInt(storedEvent.at(-6).children[0].lastChild.style.background.slice(10, 12)) === parseInt(prevEvent.children[0].lastChild.style.background.slice(10, 12))) {
        memorized.push(storedEvent.at(-6).id)
        memorized.push(prevEvent.id)
        clickCounter = 0
        if (shuffledGifs.length === memorized.length) {
          const div = document.createElement('div')
          const titleCon = document.createElement('div')
          const h1 = document.createElement('h1')
          const button = document.createElement('button')
          const secH1 = document.createElement('h1')

          if (localStorage.getItem('highest') === null || score < parseInt(localStorage.getItem('highest'))) {
            localStorage.setItem('highest', score)
            h1.textContent = 'Congrats you got the best score.'
            secH1.textContent = 'Your score was ' + score
          } else {
            h1.textContent = 'Congrats you finished the game.'
            secH1.textContent = 'Your score ' + score
          }

          highest.textContent = localStorage.getItem('highest')

          div.classList.add('begin')

          titleCon.classList.add('title-con')

          h1.classList.add('title')

          secH1.classList.add('title')
          secH1.classList.add('sec-title')

          button.classList.add('finish')
          button.textContent = 'Play again?'

          gameContainer.textContent = ''

          button.addEventListener('click', (e) => {
            gameContainer.textContent = ''
            shuffledGifs = shuffle(GIFS)
            createDivForGifs(shuffledGifs)
            memorized = []
            score = 0
            currScore.textContent = score
          })

          titleCon.appendChild(h1)
          titleCon.appendChild(secH1)
          div.appendChild(titleCon)
          div.appendChild(button)

          gameContainer.appendChild(div)
        }
      } else {
        setTimeout(() => {
          storedEvent.at(-6).children[0].style.transform = 'rotateY(0deg)'
          prevEvent.children[0].style.transform = 'rotateY(0deg)'
          clickCounter = 0
          prevEvent = null
        }, 1000)
      }
    }
  }
}

// when the DOM loads

// createDivsForColors(shuffledColors)

// createDivForGifs(shuffledGifs)

// if (shuffledGifs.length === memorized.length) {
//   const h1 = document.createElement('h1')
//   const button = document.createElement('button')

//   h1.textContent = 'Congrats you finished the game ' + score

//   button.classList.add('reset')
//   button.textContent = 'Play again?'

//   gameContainer.textContent = ''
//   gameContainer.appendChild(h1)
//   gameContainer.appendChild(button)
// }

reset.addEventListener('click', (e) => {
  gameContainer.textContent = ''
  shuffledGifs = shuffle(GIFS)
  createDivForGifs(shuffledGifs)
  memorized = []
  score = 0
  currScore.textContent = score
})

document.querySelector('.finish').addEventListener('click', () => {
  gameContainer.textContent = ''
  shuffledGifs = shuffle(GIFS)
  createDivForGifs(shuffledGifs)
  memorized = []
  score = 0
  currScore.textContent = score
  reset.classList.remove('hide')
  document.querySelector('.score-con').classList.remove('hide')
})

highest.textContent = localStorage.getItem('highest') || '0'